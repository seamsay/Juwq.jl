using Juwq

using Documenter
using Test

@testset "All" begin
    include("unit.jl")
    include("documentation.jl")
    include("integration.jl")
end
