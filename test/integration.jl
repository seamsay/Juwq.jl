# TODO: Do integration tests via subprocesses, since that's how the program will be used.
@testset "Integration" begin
    function runner(program, input)
        old_stdin = stdin
        old_stdout = stdout

        (_, writer) = redirect_stdin()
        (reader, _) = redirect_stdout()

        try
            write(writer, input)
            close(writer)

            Juwq.CLI.run(program)

            String(readavailable(reader))
        finally
            redirect_stdin(old_stdin)
            redirect_stdout(old_stdout)
        end
    end

    @test runner(raw"""@start (_) -> println("hello")""", "") == "hello\n"
    @test runner(raw"""@finish () -> println("goodbye")""", "") == "goodbye\n"

    @test runner(
        raw"""@record (line, _) -> println("always: $line")""",
        "here are\ntwo lines",
    ) == "always: here are\nalways: two lines\n"

    @test runner(
        raw"""@record r"are" (line, _) -> println("only on 'are': $line")""",
        "here are\ntwo lines",
    ) == "only on 'are': here are\n"

    @test runner(
        raw"""
        @record r"SWITCH" function (_, config)
            config.rs = '|'
            config.fs = '\t'
        end
        @record (record, _) -> if !occursin("SWITCH", record[0]) println(record[2]) end
        """,
        """these records are
        newline whitespace delimited
        SWITCH
        these\trecords\tare|pipe\ttab\tdelimited
        """,
    ) == "records\nwhitespace\nrecords\ntab\n"
end
