@testset "Unit" begin
    @testset "@pipe" begin
        foo(a = 1, b = 2, c = 3; d = 4, e = 5) = "$a$b$c$d$e"

        @test 9 |> Juwq.@pipe(foo()) == "92345"
        @test 9 |> Juwq.@pipe(foo(8)) == "98345"
        @test 9 |> Juwq.@pipe(foo(8, 7)) == "98745"
        @test 9 |> Juwq.@pipe(foo(8, 7, d = 6)) == "98765"

        @test 9 |> Juwq.@pipe(1, foo()) == "92345"
        @test 9 |> Juwq.@pipe(1, foo(8)) == "98345"
        @test 9 |> Juwq.@pipe(1, foo(8, 7)) == "98745"
        @test 9 |> Juwq.@pipe(1, foo(8, 7, d = 6)) == "98765"

        @test 9 |> Juwq.@pipe(2, foo(8)) == "89345"
        @test 9 |> Juwq.@pipe(2, foo(8, 7)) == "89745"
        @test 9 |> Juwq.@pipe(2, foo(8, 7, d = 6)) == "89765"

        @test 9 |> Juwq.@pipe(3, foo(8, 7)) == "87945"
        @test 9 |> Juwq.@pipe(3, foo(8, 7, d = 6)) == "87965"

        @test 9 |> Juwq.@pipe(d, foo()) == "12395"
        @test 9 |> Juwq.@pipe(d, foo(8)) == "82395"
        @test 9 |> Juwq.@pipe(d, foo(8, 7)) == "87395"
        @test 9 |> Juwq.@pipe(d, foo(8, 7, 6)) == "87695"

        @test 9 |> Juwq.@pipe(3, foo(8, 7; d = 6)) == "87965"
        @test 9 |> Juwq.@pipe(d, foo(8, 7, 6; e = 4)) == "87694"
        @test 9 |> Juwq.@pipe(d, foo(8, 7, 6, e = 4)) == "87694"
    end

    @testset "Record" begin
        @testset "Iteration" begin
            let record = Juwq.Record("This is a line!")
                @test map(uppercase, record) == ["THIS", "IS", "A", "LINE!"]
            end
        end

        @testset "Indexing" begin
            let record = Juwq.Record("This is a line.")
                @test record[0] == "This is a line."
                @test record[1] == "This"
                @test record[2] == "is"
                @test record[3] == "a"
                @test record[4] == "line."
                @test record[2:3] == ["is", "a"]
            end

            let record = Juwq.Record("this\tis\ta line of\ttab separated\tfields", '\t')
                @test record[0] == "this\tis\ta line of\ttab separated\tfields"
                @test record[1] == "this"
                @test record[2] == "is"
                @test record[3] == "a line of"
                @test record[4] == "tab separated"
                @test record[5] == "fields"
            end
        end

        @testset "Setting Indices" begin
            let record = Juwq.Record("This is a line.")
                record[0] = "Another line."
                @test record[0] == "Another line."
                @test record[1] == "Another"
                @test record[2] == "line."
            end

            let record = Juwq.Record("This is a line.")
                Juwq.renew!(record, "Another line.")
                @test record[0] == "Another line."
                @test record[1] == "Another"
                @test record[2] == "line."
            end

            let record = Juwq.Record("This is a line.")
                record[2] = "be"
                @test record[0] == "This be a line."
                @test record[1] == "This"
                @test record[2] == "be"
                @test record[3] == "a"
                @test record[4] == "line."
            end

            let record = Juwq.Record("This is a line.")
                record[2] = "is definitely"
                @test record[0] == "This is definitely a line."
                @test record[1] == "This"
                @test record[2] == "is definitely"
                @test record[3] == "a"
                @test record[4] == "line."
            end

            let record = Juwq.Record("This is a line.")
                record[2] = "isn't"
                record[3] = "not a"
                @test record[0] == "This isn't not a line."
                @test record[1] == "This"
                @test record[2] == "isn't"
                @test record[3] == "not a"
                @test record[4] == "line."
            end

            let record = Juwq.Record("This is a line.")
                record[3] = ""
                @test record[0] == "This is  line."
                @test record[1] == "This"
                @test record[2] == "is"
                @test record[3] == ""
                @test record[4] == "line."
            end

            let record = Juwq.Record("This  is  a  line.")
                record[2] = ""
                record[2] = "isn't not"
                @test record[0] == "This  isn't not  a  line."
                @test record[1] == "This"
                @test record[2] == "isn't not"
                @test record[3] == "a"
                @test record[4] == "line."
            end

            let record = Juwq.Record("This  is  a  line.")
                record[1] = ""
                record[1] = "That"
                @test record[0] == "That  is  a  line."
                @test record[1] == "That"
                @test record[2] == "is"
                @test record[3] == "a"
                @test record[4] == "line."
            end

            let record = Juwq.Record("This  is  a  line.")
                record[4] = ""
                record[4] = "sentence."
                @test record[0] == "This  is  a  sentence."
                @test record[1] == "This"
                @test record[2] == "is"
                @test record[3] == "a"
                @test record[4] == "sentence."
            end

            let record = Juwq.Record("This is a line.")
                record[2:3] .= ""
                @test record[0] == "This   line."
                @test record[1] == "This"
                @test record[2] == ""
                @test record[3] == ""
                @test record[4] == "line."
            end

            let record = Juwq.Record("This is a line.")
                record[2] = record[3] = ""
                @test record[0] == "This   line."
                @test record[1] == "This"
                @test record[2] == ""
                @test record[3] == ""
                @test record[4] == "line."
            end

            let record = Juwq.Record("This is a line.")
                record[3] = ""
                Juwq.renew!(record, record[0])
                @test record[0] == "This is  line."
                @test record[1] == "This"
                @test record[2] == "is"
                @test record[3] == "line."
            end
        end

        @testset "Replacement" begin
            let record = Juwq.Record("This is a line.")
                record[3] = ""
                Juwq.squash!(record)
                @test record[0] == "This is line."
                @test record[1] == "This"
                @test record[2] == "is"
                @test record[3] == "line."
            end

            let record = Juwq.Record("This is a line.")
                replace!(record, "i" => "u")
                @test record[0] == "Thus us a lune."
                @test record[1] == "Thus"
                @test record[2] == "us"
                @test record[3] == "a"
                @test record[4] == "lune."
            end

            let record = Juwq.Record("This is a line.")
                record[3] = ""
                replace!(record, r"\s+" => "\t")
                @test record[0] == "This\tis\tline."
                @test record[1] == "This"
                @test record[2] == "is"
                @test record[3] == "line."
            end

            let record = Juwq.Record("line\tof\ttab separated\tfields", '\t')
                record[3] = ""
                replace!(record, r"\s+" => "\t")
                @test record[0] == "line\tof\tfields"
                @test record[1] == "line"
                @test record[2] == "of"
                @test record[3] == "fields"
            end

            let record = Juwq.Record("line\tof  very    crazy\ttab separated\tfields", '\t')
                record[3] = ""
                replace!(record, r"(\s)+" => s"\1")
                @test record[0] == "line\tof very crazy\tfields"
                @test record[1] == "line"
                @test record[2] == "of very crazy"
                @test record[3] == "fields"
            end
        end
    end
end
