using Documenter, Juwq

DocMeta.setdocmeta!(Juwq, :DocTestSetup, :(using Juwq))

makedocs(;
    modules = [Juwq],
    format = Documenter.HTML(),
    pages = ["Home" => "index.md"],
    repo = "https://gitlab.com/seamsay/Juwq.jl/blob/{commit}{path}#{line}",
    sitename = "Juwq.jl",
    authors = "Sean Marshallsay",
)
