using JuliaFormatter

const KWARGS = Dict(
    :always_for_in => true,
    :whitespace_typedefs => true,
    :whitespace_ops_in_indices => true,
    :remove_extra_newlines => true,
)

const ROOT = dirname(dirname(@__DIR__))

function notformatted(path)
    if isfile(path)
        if last(splitext(path)) != ".jl" || success(`git check-ignore $path`)
            return []
        end

        original = read(path, String)
        formatted = format_text(original; KWARGS...)

        if original != formatted
            [path]
        else
            []
        end
    else
        notformatted(joinpath.(path, readdir(path)))
    end
end

notformatted(paths::Vector) = collect(Iterators.flatten(notformatted.(paths)))

if !isempty(ARGS)
    length(ARGS) == 1 && ARGS[1] == "--check" || error("Bad arguments: $ARGS")

    not_formatted_files = notformatted(ROOT)

    if !isempty(not_formatted_files)
        println(stderr, "The following files were not formatted:")

        for file in not_formatted_files
            println(stderr, "\t$file")
        end

        exit(1)
    end
else
    format(ROOT; KWARGS...)
end
