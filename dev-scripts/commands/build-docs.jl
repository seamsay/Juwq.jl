using Pkg

cd(dirname(dirname(@__DIR__)))

Pkg.activate("docs")
Pkg.develop(PackageSpec(path = pwd()))
Pkg.instantiate()

include(joinpath(abspath("docs"), "make.jl"))
