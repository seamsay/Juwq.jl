# Juwq

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://seamsay.gitlab.io/Juwq.jl/dev)
[![Build Status](https://gitlab.com/seamsay/Juwq.jl/badges/master/pipeline.svg)](https://gitlab.com/seamsay/Juwq.jl/pipelines)
[![Coverage](https://gitlab.com/seamsay/Juwq.jl/badges/master/coverage.svg)](https://gitlab.com/seamsay/Juwq.jl/commits/master)

Juwq ([UK](https://en.wikipedia.org/wiki/British_English): [/**dʒɜːk**/](https://en.wikipedia.org/wiki/Help:IPA/English), [US](https://en.wikipedia.org/wiki/American_English): [/**dʒuk**, **dju:k**/](https://en.wikipedia.org/wiki/Help:IPA/English), [Scotland](https://en.wikipedia.org/wiki/Scottish_English): [/**jʊx**/](https://en.wikipedia.org/wiki/Help:IPA/Scottish_Gaelic)) is a command-line utility that aims to replace Julia users' use of [AWK](https://www.gnu.org/software/gawk/) and [jq](https://stedolan.github.io/jq/) with Julia code. It does this by providing an AWK-_like_ environment in which to define functions that will be run on each line of the input, and utility functions and macros that replicate the functionality of AWK and jq.