struct Environment
    start::Vector{Function}
    record::Dict{Regex, Vector{Function}}
    finish::Vector{Function}
end

Environment() = Environment([], Dict(), [])

"""
User editable that affects how the rest of the stream will be processed.
"""
mutable struct Configuration
    field_separator::Any
    record_separator::Any
end

Configuration() = Configuration(nothing, '\n')

proper_ty_name(name) =
    if name === :fs
        :field_separator
    elseif name === :rs
        :record_separator
    else
        name
    end

Base.getproperty(config::Configuration, name::Symbol) =
    invoke(Base.getproperty, Tuple{Any, Symbol}, config, proper_ty_name(name))
Base.setproperty!(config::Configuration, name::Symbol, value) =
    invoke(Base.setproperty!, Tuple{Any, Symbol, Any}, config, proper_ty_name(name), value)

function start(environment, func)
    push!(environment.start, func)
end

macro start(func)
    quote
        Juwq.start($func)
    end
end

function finish(environment, func)
    push!(environment.finish, func)
end

macro finish(func)
    quote
        Juwq.finish($func)
    end
end

function record(environment, regex, func)
    if !haskey(environment.record, regex)
        environment.record[regex] = []
    end

    push!(environment.record[regex], func)
end

macro record(regex, func)
    quote
        Juwq.record($regex, $func)
    end
end

macro record(func)
    quote
        Juwq.record(r".*", $func)
    end
end

function setup_environment!(macroexpanded, environment)
    insert!(macroexpanded.args, 1, quote
        const $environment = Juwq.Environment()
    end)
end

"
Insert an environment as the first argument to any call to the relevent functions.

`Juwq.start`, `Juwq.record`, and `Juwq.finish` all need an environment as their first argument to make them threadsafe. This function will ensure any call to those functions get the environment as their first argument.
"
function insert_environment!(macroexpanded, environment)
    stack = [macroexpanded]
    while !isempty(stack)
        expression = pop!(stack)

        if expression.head === :call
            path = expression.args[1]
            if path isa Expr && path.head === :.
                ref = path.args[1]
                func = path.args[2]
                if ref isa GlobalRef &&
                   ref.name === :Juwq &&
                   func isa QuoteNode &&
                   (
                       func.value === :start ||
                       func.value === :record ||
                       func.value === :finish
                   )
                    insert!(expression.args, 2, environment)
                end
            end
        end

        for argument in expression.args
            if argument isa Expr
                push!(stack, argument)
            end
        end
    end
end
