"
Module in which user supplied scripts are `eval`ed.
"
module Runtime

using ..Juwq
using Juwq: @finish, @pipe, @record, @start

end # module
