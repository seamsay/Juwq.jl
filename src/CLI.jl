module CLI

using ..Juwq
using ..Runtime

"
Main program loop.
"
# TODO: How to specify inputs and outputs?
function run(program)
    # NOTE: If we don't do this then Julia can't parse expressions on separate lines.
    # TODO: Is this a bug in Julia worth raising?
    program = "begin $program end"
    expression = macroexpand(Runtime, Meta.parse(program))

    @gensym environment
    Juwq.setup_environment!(expression, environment)
    Juwq.insert_environment!(expression, environment)

    Runtime.eval(expression)

    configuration = Juwq.Configuration()
    environment = @eval Runtime.$environment

    for func in environment.start
        Base.invokelatest(func, configuration)
    end

    while !eof(stdin)
        # TODO: Allow `record_separator` to be a regex or even arbitrary function.
        record = Juwq.Record(
            readuntil(stdin, configuration.record_separator),
            configuration.field_separator,
        )

        for (regex, funcs) in environment.record
            # TODO: Have some way to have a negative match.
            if match(regex, record[0]) !== nothing
                for func in funcs
                    Base.invokelatest(func, record, configuration)
                end
            end
        end
    end

    for func in environment.finish
        Base.invokelatest(func)
    end
end

"
Parse command-record arguments and run the main loop.
"
function main()
    @assert length(ARGS) == 1

    run(ARGS[1])
end

end # module
