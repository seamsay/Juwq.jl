function pipe(argument, call)
    @assert call.head === :call "this macro must be used on a function call"

    # If a semicolon is used to separate positional and keyword arguments, all keyword arguments will be gathered into one `parameters` expression and placed as the first argument to the function.
    # This means that positional arguments must be inserted one position later and keyword arguments must be pushed onto the args of the `parameters` expression.

    insert_argument_without_semicolon!(args, argument::Integer, name) =
        insert!(args, argument + 1, name)
    insert_argument_without_semicolon!(args, argument::Symbol, name) =
        push!(args, Expr(:kw, argument, name))

    insert_argument_with_semicolon!(args, argument::Integer, name) =
        insert_argument_without_semicolon!(args, argument + 1, name)
    insert_argument_with_semicolon!(args, argument::Symbol, name) =
        insert_argument_without_semicolon!(args[2].args, argument, name)

    @gensym name

    if length(call.args) >= 2 && call.args[2] isa Expr && call.args[2].head === :parameters
        insert_argument_with_semicolon!(call.args, argument, name)
    else
        insert_argument_without_semicolon!(call.args, argument, name)
    end

    quote
        $(esc(name)) -> $(esc(call))
    end
end

macro pipe(argument, call)
    pipe(argument, call)
end

macro pipe(call)
    pipe(1, call)
end
