"""
A struct for conveniently indexing into a record of fields.

`Record(some_string)[i]` mostly behaves like `split(some_string)[i]` with some notable exceptions.

The zeroth element will return the full string, unsplit. Setting this element will update all other elements.

```jldoctest
julia> record = Juwq.Record("This is a line of text.")
6-element Juwq.Record:
 "This"
 "is"
 "a"
 "line"
 "of"
 "text."

julia> record[0]
"This is a line of text."

julia> record[0] = "A different line of text."; record
5-element Juwq.Record:
 "A"
 "different"
 "line"
 "of"
 "text."
```

Setting a particular field will update the whole record, correctly accounting for spaces.

```jldoctest
julia> record = Juwq.Record("This is a        line of text.")
6-element Juwq.Record:
 "This"
 "is"
 "a"
 "line"
 "of"
 "text."

julia> record[3] = "the"; record
6-element Juwq.Record:
 "This"
 "is"
 "the"
 "line"
 "of"
 "text."

julia> record[4] = "whole"; record
6-element Juwq.Record:
 "This"
 "is"
 "the"
 "whole"
 "of"
 "text."

julia> record[5] = ""; record
6-element Juwq.Record:
 "This"
 "is"
 "the"
 "whole"
 ""
 "text."

julia> record[0]
"This is the        whole  text."
```

A separate delimiter can be specified if necessary (see the [Julia manual for `split`](https://docs.julialang.org/en/v1/base/strings/#Base.split)).

```jldoctest
julia> Juwq.Record("this\tis a\ttab-separated\tline\tof fields", '\t')
5-element Juwq.Record:
 "this"
 "is a"
 "tab-separated"
 "line"
 "of fields"
```

You can use [`squash!`](@ref) to collapse duplicate spaces.

```jldoctest
julia> record = Juwq.Record("This is a        line of text."); println(record)
This is a        line of text.

julia> Juwq.squash!(record); println(record)
This is a line of text.
```
"""
mutable struct Record <: AbstractArray{String, 1}
    delimiter::Any
    record::String
    fields::Vector{SubString{String}}
    # Zero-length `SubString`s don't track their position (the always have an `offset` of 0), so it must be done manually.
    positions::Vector{NTuple{2, Int}}

    function Record(text, delimiter = nothing)
        record = new()
        record.delimiter = delimiter

        renew!(record, text)
    end
end

"""
Print the record as if it were a string.

```jldoctest
julia> println(Juwq.Record("This is a line."))
This is a line.
```
"""
Base.show(io::IO, record::Record) = print(io, record.record)

"""
Replace the whole record with another.

```jldoctest
julia> record = Juwq.Record("This is a line of text.")
6-element Juwq.Record:
 "This"
 "is"
 "a"
 "line"
 "of"
 "text."

julia> Juwq.renew!(record, "This is a different line of text."); record
7-element Juwq.Record:
 "This"
 "is"
 "a"
 "different"
 "line"
 "of"
 "text."
```

This is equivalent to setting the zeroth element.

```jldoctest
julia> record = Juwq.Record("This is a line of text.")
6-element Juwq.Record:
 "This"
 "is"
 "a"
 "line"
 "of"
 "text."

julia> record[0] = "This is a different line of text."; record
7-element Juwq.Record:
 "This"
 "is"
 "a"
 "different"
 "line"
 "of"
 "text."
```

Renewing the record with it's zeroth element is a handy way of getting rid of empty fields if you _don't_ want to get rid of the extra whitespace (use [`squash!`](@ref) if you do).

```jldoctest
julia> record = Juwq.Record("This is a line."); record[3] = ""; record
4-element Juwq.Record:
 "This"
 "is"
 ""
 "line."

julia> println(record)
This is  line.

julia> Juwq.renew!(record, record[0]); record
3-element Juwq.Record:
 "This"
 "is"
 "line."

julia> println(record)
This is  line.
```
"""
function renew!(record::Record, text)
    fields = if record.delimiter === nothing
        split(text; keepempty = false)
    else
        split(text, record.delimiter; keepempty = false)
    end

    positions =
        map(field -> (1 + field.offset, 1 + field.offset + field.ncodeunits - 1), fields)

    record.record = text
    record.fields = fields
    record.positions = positions

    record
end

Base.IndexStyle(::Type{Record}) = IndexLinear()
Base.size(record::Record) = size(record.fields)
Base.getindex(record::Record, index::Integer) =
    index == 0 ? record.record : record.fields[index]
function Base.setindex!(record::Record, value::String, index::Integer)
    if index == 0
        renew!(record, value)
    else
        difference = length(record.fields[index]) - length(value)

        start, finish = record.positions[index]
        head = record.record[1:(start - 1)]
        tail = record.record[(finish + 1):end]

        new = string(head, value, tail)
        fields = similar(record.fields)
        positions = similar(record.positions)

        for i in 1:(index - 1)
            start, finish = record.positions[i]

            fields[i] = SubString(new, start, finish)
            positions[i] = (start, finish)
        end

        let
            start, finish = record.positions[index]
            finish -= difference

            fields[index] = SubString(new, start, finish)
            positions[index] = (start, finish)
        end

        for i in (index + 1):lastindex(record.fields)
            start, finish = record.positions[i]
            start -= difference
            finish -= difference

            fields[i] = SubString(new, start, finish)
            positions[i] = (start, finish)
        end

        record.record = new
        record.fields = fields
        record.positions = positions

        record
    end
end

Base.replace!(record::Record, replacement::Pair; count = nothing) =
    renew!(record, if count === nothing
        replace(record.record, replacement)
    else
        replace(record.record, replacement; count = count)
    end)

"""
Squash multiple occurrences of whitespace into a single space.

```jldoctest
julia> record = Juwq.Record("This is a        line of text."); println(record)
This is a        line of text.

julia> Juwq.squash!(record); println(record)
This is a line of text.
```

If more complex replacements or a different character are needed, use `replace!` directly.
"""
squash!(record::Record) = replace!(record, r"\s+" => " ")
